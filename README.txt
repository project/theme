There are a lot of placeholder functions in Drupal called theme_[element]. These
are called whenever something called [element] is going to be themed, usually by
using the theme('[element]', $variables) function.

The theme() function looks for modules, themes, theme engines who wants to theme
the element in question, and uses the most specific choice available. If no
valid choice is available, it will fall back to the theme_[element] function,
which should be declared by Drupal core or the module introducing the element in
question.

If your website were to have a module called "theme", Drupal could end up being
utterly confused about the theme_[element] function, since modules replace these
fallback functions by declaring an own function with the name
[modulename]_[element]. When modulename is the same as the fallback name,
strange things may happen.

Don't turn on this module. It shouldn't give you any lasting devastations on
the site, since themes shouldn't change any data being stored, but it is
possible that Drupal would be really confused and not able to run through the
theme layer at all. It is very likely that you won't be able to view the page
where you disable modules – and in worst case you'd have to figure out how to
disable the module *and* clear Drupal's theme cache without using any web
interface.

If you find that kind of things amusing, consider joining the Drush development
group at http://groups.drupal.org/drush.
